/*
 * TODO: Enter copyright statement here.
 */
package headlessformpresentation.test;

import nl.gx.product.headlessformpresentation.Activator;
import nl.gx.product.headlessformpresentation.api.WCBConstants;

import nl.gx.webmanager.wcb.ComponentBundleDefinition;
import nl.gx.webmanager.wcb.ComponentDefinition;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * This class contains the JUnit test of the presentation component.
 * It contains some basic tests, extend it to your need.
 */
public class PresentationTest extends Activator {
    /**
     * Test if the activator defines proper component definitions and that the ids
     * are defined properly.
     */
    @Test
    public void testActivator() {
        ComponentBundleDefinition definition = getBundleDefinition();
        assertEquals(WCBConstants.BUNDLE_ID, definition.getId());
        assertEquals(WCBConstants.DOMAIN + "." + WCBConstants.WCB_ID, definition.getId());
        assertTrue(Activator.class.getPackage().getName().startsWith(WCBConstants.DOMAIN + "." + WCBConstants.WCB_ID));
        final ComponentDefinition[] defs = definition.getComponentDefinitions();
        assertTrue(defs != null);
        assertTrue(defs.length > 0);
        for (ComponentDefinition compDef : defs) {
            assertTrue(compDef.getId().startsWith(definition.getId()));
        }
    }
}
