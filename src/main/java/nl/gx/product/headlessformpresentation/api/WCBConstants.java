/*
 * TODO: Enter copyright statement here.
 */
package nl.gx.product.headlessformpresentation.api;

/**
 * This class contains the identifiers, namespaces, names and descriptions of the WCB itself and of
 * the components it contains. If you need to use IDs defined by this WCB, use these constants rather
 * then hard coded values.
 */
public final class WCBConstants {
    /**
     * The domain of this WCB, i.e. 'nl.gx.product'. By convention the domain contains only characters
     * in the range [a-z] separated by dots.
     */
    public static final String DOMAIN = "nl.gx.product";

    /**
     * The WCB ID of this WCB, i.e. 'helloworldpresentation'. By convention the WCB ID contains only characters
     * in the range [a-z].
     */
    public static final String WCB_ID = "headlessformpresentation";

    /**
     * The full namespace URI used by JCR node types contained by this WCB.
     */
    public static final String NAMESPACE_URI = "http://www.wcmexchange.com/" + WCB_ID;

    /**
     * The ID of the component bundle definition. Each WCB has exactly one component bundle definition but
     * may have one or more component definitions.
     */
    public static final String BUNDLE_ID = DOMAIN + "." + WCB_ID;

    /**
     * The name of this WCB, equals the name of the component bundle definition.
     */
    public static final String BUNDLE_NAME = "headlessformpresentation";

    /**
     * The description of this WCB, equals the description of the component bundle definition.
     */
    public static final String BUNDLE_DESCRIPTION = "headlessformpresentation WCB";

    /**
     * The component definition ID of the presentation component.
     */
    public static final String PRESENTATION_COMPONENT_ID = BUNDLE_ID + ".presentation";

    /**
     * The name of the presentation component.
     */
    public static final String PRESENTATION_COMPONENT_NAME = "headlessformpresentation presentation";

    /**
     * The description of the presentation component.
     */
    public static final String PRESENTATION_COMPONENT_DESCRIPTION = "headlessformpresentation presentation component";

    /**
     * The component definition ID of the secure forms signing servlet component.
     */
    public static final String SECURE_FORMS_SIGNING_SERVLET_COMPONENT_ID = BUNDLE_ID + ".secureformssigningservlet";

    /**
     * The name of the secure forms signing servlet component.
     */
    public static final String SECURE_FORMS_SIGNING_SERVLET_COMPONENT_NAME = "Secure forms signing servlet";

    /**
     * The description of the secure forms signing servlet component.
     */
    public static final String SECURE_FORMS_SIGNING_SERVLET_COMPONENT_DESCRIPTION = "Secure forms signing servlet";


    /**
     * Constructor. Do not instantiate, this class only contains constants.
     */
    private WCBConstants() {
    }
}