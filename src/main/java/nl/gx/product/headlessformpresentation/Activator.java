/*
 * TODO: Enter copyright statement here.
 */
package nl.gx.product.headlessformpresentation;

import nl.gx.product.headlessformpresentation.api.WCBConstants;
import nl.gx.product.headlessformpresentation.servlet.SecureFormsSigningServlet;
import nl.gx.webmanager.wcb.ComponentBundleDefinition;
import nl.gx.webmanager.wcb.ComponentDefinition;
import nl.gx.webmanager.wcb.ComponentDependency;
import nl.gx.webmanager.wcb.foundation.ComponentBundleActivatorBase;
import nl.gx.webmanager.wcb.foundation.ComponentBundleDefinitionImpl;
import nl.gx.webmanager.wcb.presentationtype.PresentationComponent;
import nl.gx.webmanager.wcb.presentationtype.PresentationComponentType;
import nl.gx.webmanager.wcb.presentationtype.impl.PresentationComponentDefinitionImpl;
import nl.gx.webmanager.wcb.presentationtype.impl.SimplePresentationComponent;
import nl.gx.webmanager.wcb.servlettype.ServletComponent;
import nl.gx.webmanager.wcb.servlettype.ServletComponentType;
import nl.gx.webmanager.wcb.servlettype.impl.ServletComponentDefinitionImpl;
import nl.gx.webmanager.wcb.servlettype.impl.SimpleServletComponent;

import java.util.Hashtable;

/**
 * Activator class of the presentation WCB. This class defines the component definitions and is responsible for
 * instantiating the WCB and its components.
 */
public class Activator extends ComponentBundleActivatorBase {
    /**
     * Creates and returns the bundle definition of the WebManager component bundle.
     *
     * @return the bundle definition of the WebManager component bundle
     */
    @Override
    protected ComponentBundleDefinition getBundleDefinition() {
        ComponentBundleDefinitionImpl componentBundleDefinition = new ComponentBundleDefinitionImpl();
        componentBundleDefinition.setId(WCBConstants.BUNDLE_ID);
        componentBundleDefinition.setName(WCBConstants.BUNDLE_NAME);
        componentBundleDefinition.setNameSpace(WCBConstants.NAMESPACE_URI);
        componentBundleDefinition.setDescription(WCBConstants.BUNDLE_DESCRIPTION);
        componentBundleDefinition.setComponentDefinitions(getComponentDefinitions());
        return componentBundleDefinition;
    }

    /**
     * Returns an array of component definitions of this bundle.
     *
     * @return Array of component definitions of this bundle
     */
    private ComponentDefinition[] getComponentDefinitions() {
        ComponentDefinition[] componentDefinitions = {
                getPresentationComponentDefinition(),
                getSecureFormsSigningServletComponentDefinition()
        };
        return componentDefinitions;
    }

    /**
     * Returns the component definition for the presentation component.
     *
     * @return The component definition for the presentation component
     */
    protected ComponentDefinition getPresentationComponentDefinition() {
        PresentationComponentDefinitionImpl definition = new PresentationComponentDefinitionImpl(false);
        definition.setId(WCBConstants.PRESENTATION_COMPONENT_ID);
        definition.setName(WCBConstants.PRESENTATION_COMPONENT_NAME);
        definition.setDescription(WCBConstants.PRESENTATION_COMPONENT_DESCRIPTION);
        definition.setTypeId(PresentationComponentType.class.getName());
        definition.setProperties(new Hashtable<String, String>());
        definition.setImplementationClassName(SimplePresentationComponent.class.getName());
        definition.setInterfaceClassNames(new String[]{PresentationComponent.class.getName()});
        definition.setDependencies(new ComponentDependency[]{});
        definition.setWrapperClassNames(new String[]{});

        return definition;
    }

    /**
     * Creates and returns the secure forms signing servlet component definition.
     *
     * @return The servlet component definition.
     */
    private ServletComponentDefinitionImpl getSecureFormsSigningServletComponentDefinition() {
        ServletComponentDefinitionImpl definition = new ServletComponentDefinitionImpl(false);
        definition.setId(WCBConstants.SECURE_FORMS_SIGNING_SERVLET_COMPONENT_ID);
        definition.setName(WCBConstants.SECURE_FORMS_SIGNING_SERVLET_COMPONENT_NAME);
        definition.setDescription(WCBConstants.SECURE_FORMS_SIGNING_SERVLET_COMPONENT_DESCRIPTION);
        definition.setTypeId(ServletComponentType.class.getName());
        definition.setProperties(new Hashtable<>());
        definition.setImplementationClassName(SimpleServletComponent.class.getName());
        definition.setInterfaceClassNames(new String[]{ServletComponent.class.getName()});
        definition.setDependencies(new ComponentDependency[]{});
        definition.setInstanceClassName(SecureFormsSigningServlet.class.getName());
        definition.setServletUrlPath("secureformssigner");

        return definition;
    }
}