/*
 * Copyright (C) 1998 - 2018 GX Software B.V. All rights reserved. The contents of this
 * work is confidential. It is prohibited to copy, duplicate or alter this work in any
 * manner without the express prior written approval of GX Software B.V.
 */
package nl.gx.product.headlessformpresentation.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Servlet that calculates the form signature based on a list of input field names.
 * It uses the nl.gx.filter.signing.FormSigner class from XperienCentral.
 */
public class SecureFormsSigningServlet extends HttpServlet {
    private static final long serialVersionUID = 8915388309994688037L;
    private static final Logger LOG = Logger.getLogger(SecureFormsSigningServlet.class.getName());

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        String signature = "";

        Object formSigner = getServletConfig().getServletContext().getAttribute("formsigner");
        if (formSigner != null) {
            String inputNamesParameter = request.getParameter("inputNames");
            if (inputNamesParameter != null && !inputNamesParameter.isEmpty()) {
                Set<String> inputNames = new HashSet<>(Arrays.asList(inputNamesParameter.split(",")));

                try {
                    // Use reflection because the FormSigner class is not exposed in the springmvc-servlet.xml
                    Method method = formSigner.getClass().getMethod("getSignature", Set.class, HttpSession.class);
                    signature = (String) method.invoke(formSigner, inputNames, request.getSession());
                } catch (SecurityException | ReflectiveOperationException ex) {
                    LOG.log(Level.WARNING, "Unable to calculate form signature", ex);
                }
            } else {
                LOG.warning("Unable to calculate form signature because no inputNames parameter is present in the request.");
            }
        } else {
            LOG.warning("Unable to calculate form signature because no form signer is present in the servlet context.");
        }

        try {
            response.getWriter().print(signature);
        } catch (Exception e) {
            LOG.log(Level.WARNING, "Could not print secure forms signature to response.", e);
        }
    }
}
