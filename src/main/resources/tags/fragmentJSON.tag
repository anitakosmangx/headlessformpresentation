<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.gx.nl/taglib/wm" prefix="wm" %>
<%@ taglib uri="http://www.gx.nl/taglib/functions" prefix="wmfn" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/nl_gx_forms_wmpformelement"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags/nl_gx_product_headlessformpresentation" %>
<%@ attribute name="fragment" required="true" type="java.lang.Object" %>
<%@ attribute name="type" required="true" type="java.lang.String" %>
<%@ attribute name="session" required="false" type="java.lang.Object" %>
<%@ attribute name="formVersion" required="false" type="java.lang.Object" %>
<%@ attribute name="hidden" required="false" type="java.lang.Boolean" %>

<c:set var="identifier" value="${fragment.identifier}" />
<c:set var="nestedPath" value="${fragment.nestedPath}" />
<c:set var="scope" value="${formScope[nestedPath]}" />

<json:object>
	<json:property name="id" value="${identifier}" />
	<json:property name="path" value="${nestedPath}" />
	<json:property name="type" value="${type}" />
	<json:property name="label" value="${wmfn:escapeToHTML(fragment.name)}" />
	<json:property name="required" value="${wmfn:instanceOf(fragment, 'nl.gx.forms.wmpformapi.api.form.FormInputFragment') && fragment.required}" />
	<json:property name="visible" value="${!hidden && (empty scope.condition || scope.condition)}" />

	<c:if test="${wmfn:instanceOf(fragment, 'nl.gx.forms.wmpformapi.api.BasicFormInputFragment')}">
		<json:property name="extraText" value="${wmfn:escapeToHTML(fragment.extraText)}" />
		<json:property name="helpText" value="${wmfn:escapeToHTML(fragment.helpText)}" />
		<forms:concatLists var="validators" list1="${fragment.defaultValidators}" list2="${fragment.customValidators}" />
		<json:array name="validations" items="${validators}" var="validator">
		  <json:object>
        <json:property name="id" value="${validator.formLogicComponentDefinition.identifier}" />
        <c:forEach var="parameter" items="${validator.parameters}">
          <json:property name="${parameter.parameterDefinition.identifier}" value="${parameter.value}" />
        </c:forEach>
		  </json:object>
		</json:array>
	</c:if>

	<json:property name="hasError" value="${not empty scope.errors}" />
	<tags:messagesJSON nestedPath="${nestedPath}" />

	<c:set var="preconditionExpression" value="${fragment.simplePreConditionExpression}" />
	<c:if test="${not empty preconditionExpression && not empty preconditionExpression.fragment}">
		<json:object name="precondition">
			<c:set var="preconditionFragmentPathItems" value="${fn:split(preconditionExpression.fragment, '.')}" />
			<json:property name="fragment" value="${preconditionFragmentPathItems[fn:length(preconditionFragmentPathItems) - 1]}" />
			<json:property name="operator" value="${preconditionExpression.operator eq 'EQUAL' ? 'equal' : 'not_equal'}" />
			<json:property name="value" value="${preconditionExpression.value}" />
		</json:object>
	</c:if>

  <c:choose>
    <c:when test="${wmfn:instanceOf(fragment, 'nl.gx.forms.wmpformfragments.api.FormFragmentOverview')}">
      <c:set var="allowedSteps" value="${session.allowedSteps}" />
      <c:set var="found" value="false" />
      <json:array name="value">
      <c:forEach var="step" items="${formVersion.steps}" varStatus="status">
        <c:if test="${step.identifier == param.wmstepid}">
          <c:set var="found" value="true" />
        </c:if>

        <c:if test="${!found and fn:contains(allowedSteps,step.identifier)}">
          <c:set var="escapedTitle"><forms:xmlescape value="${step.title}" /></c:set>
          <json:object>
            <json:property name="stepTitle" value="${escapedTitle}" />
            <json:array name="fields">
              <wm:render presentationName="wmaheadlesspresentation FormFragmentOverviewFragmentContainer" container="${step}" overviewNestedPath="${step.identifier}." />
            </json:array>
          </json:object>
        </c:if>
      </c:forEach>
      </json:array>
    </c:when>
    <c:when test="${wmfn:instanceOf(fragment, 'nl.gx.forms.wmpformfragments.api.FormFragmentSection')}">
      <json:array name="value">
      <c:forEach var="part" items="${fragment.compositeParts}">
        <c:forEach var="child" items="${part.fragments}">
          <wm:render object="${child}" />
        </c:forEach>
      </c:forEach>
      </json:array>
    </c:when>
    <c:when test="${wmfn:instanceOf(fragment, 'nl.gx.forms.wmpformfragments.api.FormFragmentCheckboxList')}">
	    <json:array name="value" items="${scope.values}" />
    </c:when>
    <c:when test="${wmfn:instanceOf(fragment, 'nl.gx.forms.wmpformfragments.api.FormFragmentParagraph')}">
      <c:set var="evaluatedText"><forms:xmlescape value="${fragment.text}"/></c:set>
	    <json:property name="value" value="${fragment.text}" />
    </c:when>
    <c:otherwise>
	    <json:property name="value" value="${scope.value}" />
	  </c:otherwise>
  </c:choose>

  <c:choose>
    <c:when test="${wmfn:instanceOf(fragment, 'nl.gx.forms.wmpformfragments.api.FormFragmentCheckboxList')}">
      <json:array name="items" var="item" items="${fragment.items}">
        <json:object>
          <json:property name="name" value="${item.name}"/>
          <json:property name="value" value="${item.value}"/>
        </json:object>
      </json:array>
    </c:when>
    <c:when test="${wmfn:instanceOf(fragment, 'nl.gx.forms.wmpformfragments.api.FormFragmentList')}">
      <json:object name="items">
      <c:forEach var="item" items="${fragment.items}">
        <json:property name="${item.value}" value="${item.name}"/>
      </c:forEach>
      </json:object>
    </c:when>
    <c:when test="${wmfn:instanceOf(fragment, 'nl.gx.forms.wmpformfragments.api.FormFragmentFileUpload')}">
      <json:array name="acceptedExtensions" items="${fragment.acceptedExtensions}" />
      <json:array name="acceptedMimeTypes" items="${fragment.acceptedMimeTypes}" />
    </c:when>
    <c:otherwise />
  </c:choose>

</json:object>