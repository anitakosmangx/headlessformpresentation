<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>

<%@ attribute name="nestedPath" required="false" type="java.lang.Object" %>

<c:choose>
	<c:when test="${empty nestedPath}">
		<c:set var="root" value="${formScope}" />
	</c:when>
	<c:otherwise>
		<c:set var="root" value="${formScope[nestedPath]}" />
	</c:otherwise>
</c:choose>

<c:if test="${not empty root.messages}">
	<json:array name="messages" var="message" items="${root.messages}">
		<json:object>
			<json:property name="key" value="${message.key}" />
			<json:property name="value" value="${message.value}" />
		</json:object>
	</json:array>
</c:if>

<c:if test="${not empty root.errors}">
	<json:array name="errors">
		<c:forEach var="error" items="${root.errors}">
			<json:object>
				<json:property name="key" value="${error.key}" />
				<json:property name="message" value="${error.value}" />
			</json:object>
		</c:forEach>
	</json:array>
</c:if>