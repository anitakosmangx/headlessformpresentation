================================================================================================================

                                   	R E A D M E   T E X T
										
                                    headlessformpresentation

Release 1.0.0
Last revised <Date of last revision>
-------------------------

====================================================================
CONTENTS
====================================================================

-1- LICENSE AND COPYRIGHT
-2- INTRODUCTION
-3- NEW FEATURES IN THIS RELEASE
-4- CONTENTS OF THE DISTRIBUTION PACKAGE
-5- HARDWARE AND SOFTWARE REQUIREMENTS
-6- INSTALLATION
-7- KNOWN ISSUES
-8- TROUBLESHOOTING
-9- CONTACT INFORMATION

====================================================================
-1- LICENSE AND COPYRIGHT 
====================================================================
<
	License statement if applicable. For example:

	� 2000-2010 GX Software B.V. All rights reserved. The contents of this work is confidential. 
  It is prohibited to copy, duplicate or alter this work in any manner without the express prior written approval 
  of GX Software B.V. 
>


====================================================================
-2- INTRODUCTION 
====================================================================
<
	General information about the contents of the WCA; what WCBs does it contain and what is their purpose?
>


====================================================================
-3- NEW FEATURES IN THIS RELEASE
====================================================================
<for each WCB contained by the WCA:>

The following features are new in <major WCA version> with respect to <previous major WCA version>:

1. ...
2. ...


====================================================================
-4- CONTENTS OF THE DISTRIBUTION PACKAGE
====================================================================
The WCA contains the following files and folders (of which folders are surrounded by < and >):

  FILE OR DIRECTORY                 |  DESCRIPTION
  --------------------------------------------------------------------------------------------
  readme.txt                        |  This readme file
  changelog.txt                     |  Overview including history of all issues that have been fixed
  pom.xml                           |  The pom.xml of the WCB
  <bin>                             |  The compiled WCBs
    headlessformpresentation_x.y.z.jar     |  <Description of the WCB>
  <src>                             |  Source folder of the WCB
    <main>                          |  
      <assembly>                    |  Assembly files for WCA generation
      <java>                        |  Java source files
      <resources>                   |  Resource files used by the WCB
    <test>                          |
      <java>                        |  Java unit test files
  <doc>                             |
    <apidocs>                       |  The generated javadoc   


====================================================================
-5- HARDWARE AND SOFTWARE REQUIREMENTS
====================================================================

--------------------------------------------------------------------
-5.1- APPLICATION SERVER REQUIREMENTS
--------------------------------------------------------------------
< either >
The following system requirements are applicable for the WebManager application running the WCBs contained by this WCA. 
The software has been tested thoroughly on these configurations.

Hardware requirements
---------------------
- ...
- ...

Software requirements
---------------------
- ...
- ...

< or >
No additional application server requirements apply to this WCA. For a full list of application
server requirements, see the application server requirements of the WebManager plaform.

--------------------------------------------------------------------
-5.2- DATABASE SERVER REQUIREMENTS
--------------------------------------------------------------------
< either >
The following system requirements are applicable for the database server that are used by the WCBs in this WCA to 
store content. The software has been tested thoroughly on these configurations.

Hardware requirements
---------------------
- ...
- ...

Software requirements
---------------------
- ...
- ...

< or >
No additional database server requirements apply to this WCA. For a full list of database
server requirements, see the database server requirements of the WebManager plaform.

--------------------------------------------------------------------
-5.3- GX WEBMANAGER USER DESKTOP REQUIREMENTS
--------------------------------------------------------------------
< either >
The following system requirements are applicable for an editor working with the WCBs contained by this WCA. 
The software has been tested thoroughly on these configurations.

Hardware requirements
---------------------
- ...
- ...

Software requirements
---------------------
- ...
- ...

< or >
No additional GX WebManager user desktop requirements apply to this WCA. For a full list of GX WebManager user 
desktop requirements, see the GX WebManager user desktop requirements of the WebManager plaform.

--------------------------------------------------------------------
-5.4- END USER DESKTOP REQUIREMENTS
--------------------------------------------------------------------
< either >
The following system requirements are applicable for a visitor of the website that uses functionality exposed 
by the WCBs contained by this WCA. The software has been tested thoroughly on these configurations.

Hardware requirements
---------------------
- ...
- ...

Software requirements
---------------------
- ...
- ...

< or >
No additional end user desktop requirements apply to this WCA. For a full list of end user 
desktop requirements, see the end user desktop requirements of the WebManager plaform.

====================================================================
-6- INSTALLATION
====================================================================
< either >
<Describe the additional installation steps required to install the WCBs in the WCA.>

1. ... 
2. ...

< or >
No additional installation steps are required.

====================================================================
-7- KNOWN ISSUES
====================================================================
The following issues are known issues and will be fixed in subsequent WCA releases:

1. ... 
2. ...


====================================================================
-8- TROUBLESHOOTING
====================================================================
< either >
If you run into problems using the WCA the following problem and solution descriptions might help:


Problem
-------
<Problem description>

Solution
--------
<Solution description>

====================================================================

Problem
-------
<Problem description>

Solution
--------
<Solution description>

< or >
There are currently no troubleshooting tips.

====================================================================
-9- CONTACT INFORMATION
====================================================================
< Contact information can be included here. Users might want to contact the distributor of this WCA for 
questions, license issues, logging issues, etc. This section is not mandatory.> 
              
              
